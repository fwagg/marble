﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hour {

	public int CurrentOClock; //Whole hour, the "hundred" part

	public Part currentPart; //current 15 minute interval

	public List<Part> Parts = new List<Part>(); 

	public Hour (int newOClock) {
		CurrentOClock = newOClock;
		FillParts ();
	}

	private void FillParts() {
		int i = 0;
		while (i < 45) {
			Parts.Add (new Part (i));
			i += 15;
		}
		currentPart = Parts [0];
	}


}
