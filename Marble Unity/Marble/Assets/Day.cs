﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day {

	public string dayName;
	public float conversion;
	public List<Hour> Hours = new List<Hour>();
	public Hour CurrentHour;

	public Day (string newName, float newConversion) {
		dayName = newName;
		conversion = newConversion;
		FillHours ();
	}

	private void FillHours() {
		int i = 0;
		while (i < 2400) {
			Hours.Add (new Hour (i));
			i+=100;
		}
		CurrentHour = Hours [0];
	}

	public int CurrentTime() {
		int value = CurrentHour.currentPart.Interval + CurrentHour.CurrentOClock;
		return value;
	}
		
}
