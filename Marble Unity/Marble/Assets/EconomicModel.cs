﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EconomicModel : MonoBehaviour {

	public List<Day> Days = new List<Day>();
	public Day Today;

	private int weekdayCounter = 1; //place in array for starting day of the week. 0 is Sunday.

	//TODO - make conversion rate limits dynamic
	public float conversionLow = 0.05f;
	public float conversionHigh = 0.40f;

	public void FillDays() {
		Days.Add (new Day("Sunday", Random.Range(conversionLow,conversionHigh)));
		Days.Add (new Day("Monday", Random.Range(conversionLow,conversionHigh)));
		Days.Add (new Day("Tuesday", Random.Range(conversionLow,conversionHigh)));
		Days.Add (new Day("Wednesday", Random.Range(conversionLow,conversionHigh)));
		Days.Add (new Day("Thursday", Random.Range(conversionLow,conversionHigh)));
		Days.Add (new Day("Friday", Random.Range(conversionLow,conversionHigh)));	
		Days.Add (new Day ("Saturday", Random.Range (conversionLow, conversionHigh)));
		Today = Days [weekdayCounter];
	}

	void Start () {
		if (Days.Count <= 1) {
			FillDays ();
		}
	}

	private float nextActionTime = 0.0f;
	public float period = 1.0f;

	void Update () {
		if (Time.time > nextActionTime) {
			nextActionTime = Time.time + period;
			runEvery ();
		}
	}

	private void runEvery () {

		Today = Days [weekdayCounter];

		//TODO - needs to increment by part, then by hour, then by day

		Debug.Log ("now it's " + Today.dayName + " and it's " + Today.CurrentTime());

		weekdayCounter++;

		if (weekdayCounter > 6) {
			
			weekdayCounter = 0;

		}
	}

}
